import React, { useState, useEffect } from "react";
import "./css/InputForm.css";
import axios from 'axios';
function InputForm({
  editingStudent,
  changeDataMethod,
  addNewStudentFunc,
}) {
  const [formData, setFormData] = useState(
    editingStudent != null
      ? editingStudent
      : { studentId: "_" ,name: "", birthdate: "", gender: true, facultyID: "" }
  );
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    // console.log(`${name} -- ${value}`);
    name === "gender" ?
    setFormData({
      ...formData,
      [name]: value === "nam" ? true : false,
    })
    :
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  // Post data lên api
  const handleSubmit = (event) => {
    event.preventDefault();
    addNewStudentFunc(formData);
    axios.post('https://localhost:7181/api/Student', formData)
      .then(response => {
        changeDataMethod();
      })
      .catch(error => {
        console.error("Không thể post dữ liệu");
      });
      
  };

  const handleUpdateStudent = (event) => {
    event.preventDefault();
    // addNewStudentFunc(formData);
    axios.put(`https://localhost:7181/api/Student/${formData.studentId}`, formData)
  .then(response => {
    changeDataMethod();
  })
  .catch(error => {
    console.error(error); // in ra lỗi nếu có
  });
  
  }
  // Thêm mới sv
  // const handleAddClick = () => {
  //   // formData.birthday = new Date(formData.birthday).toLocaleDateString("en-GB");
  //   addNewStudentFunc(formData);
  // };

  // cập nhật đối tượng khi ấn sửa trong danh sách sinh viên
  useEffect(() => {
    if (editingStudent != null) {
      setFormData(editingStudent);
      // const birthday = editingStudent.birthday.split("/");
      // setFormData((prevState) => ({
      //   ...prevState,
      //   birthday: `${birthday[2]}-${birthday[1]}-${birthday[0]}`,
      // }));
    }
  }, [editingStudent]);

  // const handleSaveClick = () => {
  //   // console.log(editingStudent.name);
  //   changeDataMethod(formData); // đặt lại state editingStudent về null để ẩn form
  //   // Thực hiện lưu dữ liệu với API hoặc state tại đây
  // };
  return (
    <>
      <div className="inputForm2">
        <div>
         {/* handleAddClick */}
          <button onClick={handleSubmit}>Thêm mới</button>  
          <button onClick={handleUpdateStudent}>Cập nhật</button>
          <button>Xóa</button>
        </div>

        <div />

        <div className="rowInput">
          <label htmlFor="txtMaSV" className="inputLabel">
            Mã SV<span style={{ color: "red" }}>*</span>:
          </label>
          <input
            className="rowIn"
            type="text"
            id="txtMaSV"
            name="studentId"
            value={formData.studentId}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="rowInput">
          <label htmlFor="txtTenSV" className="inputLabel">
            Tên sinh viên<span style={{ color: "red" }}>*</span>:
          </label>
          <input
            className="rowIn"
            type="text"
            id="txtTenSV"
            name="name"
            value={formData.name}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="rowInput">
          <label htmlFor="txtNgaySinh" className="inputLabel">
            Ngày sinh:
          </label>
          <input
            className="rowIn"
            type="input"
            id="txtNgaySinh"
            name="birthdate"
            value={formData.birthdate}
            onChange={handleInputChange}
          />
        </div>
        <div className="rowInput">
          <label htmlFor="gender" className="inputLabel">
            Giới tính<span style={{ color: "red" }}>*</span>:
          </label>
          <div>
            <input
              type="radio"
              id="male"
              name="gender"
              value="nam"
              checked={formData.gender === true}
              onChange={handleInputChange}
              required
            />
            <label htmlFor="male">Nam</label>
          </div>
          <div>
            <input
              type="radio"
              id="female"
              name="gender"
              value= "nu"
              checked={formData.gender === false}
              onChange={handleInputChange}
              required
            />
            <label htmlFor="female">Nữ</label>
          </div>
        </div>
        <div className="rowInput">
          <label htmlFor="faculty" className="inputLabel">
            Khoa<span style={{ color: "red" }}>*</span>:
          </label>
          <select
            className="rowIn"
            id="faculty"
            name="facultyID"
            value={formData.facultyID}
            onChange={handleInputChange}
            required
          >
            <option value="">--Chọn khoa--</option>
            <option value="KCNTT">
            Khoa Công Nghệ Thông Tin
            </option>
            <option value="KKHMT">
              Khoa Khoa học máy tính
            </option>
            <option value="KKT11">Khoa Kinh tế</option>
            <option value="KVHNT">
              Khoa Văn hóa và Nghệ thuật
            </option>
            <option value="KTH12">Khoa Toán học</option>
            <option value="KCNMT">
              Khoa Khoa học và Công nghệ Môi trường
            </option>
            <option value="KKL32">Khoa Luật</option>
            <option value="KHXHV">
              Khoa Khoa học Xã hội và Nhân văn
            </option>
          </select>
        </div>
      </div>
    </>
  );
}
export default InputForm;
