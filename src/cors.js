const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

// Các định nghĩa route của bạn ở đây

app.listen(3000, () => {
  console.log('Máy chủ đã bắt đầu lắng nghe trên cổng 3000!');
});
