import React, {  useState, useEffect } from "react";
import './css/StudentList.css';
import InputForm from "./InputForm";
import FormSearch from "./FormSearch";
import axios from 'axios';

function StudentList() {
  const [students, setStudents] = useState([]);
  const [isDataChanged, setIsDataChanged] = useState(false);
  const [searchStudents, setSearchStudents] = useState([]);
  const [editingStudent, setEditingStudent] = useState(null);
  const [flagRenderBorad,setFlagRenderBorad] = useState(true);
  let listcheckbox = [];
  let idArr = ["10001","10002","10003","10004","10005","10006","10007","10008","10009","100010","100011"];
  let nameArr = ["Dương Minh A","Dương Minh B","Dương Minh C","Dương Minh D","Dương Minh E","Dương Minh F","Dương Minh G","Dương Minh H","Dương Minh I","Dương Minh K"];
  let birthdayArr = ["01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001","01/01/2001"];


  useEffect(() => {
    getAllDataAPI();
  }, [isDataChanged]);

  const getAllDataAPI = () => {
    axios.get('https://localhost:7181/fullJoin')
    .then(response => {
      // Xử lý dữ liệu tại đây
      setStudents(response.data);
      listcheckbox = [...new Array(10)].fill(false);
    })
    .catch(error => {
      // console.log(error);
      // Xử lý lỗi tại đây
    });
  } 
  
  // Thêm 10 sinh viên ngẫu nhiên vào mảng students
  const addRandomStudents = () => {
    const newStudents = [];
    for (let i = 0; i < 10; i++) {
      const newStudent = {
        id: `${idArr[i]}${students.length}`,
        name: nameArr[i],
        birthday: birthdayArr[i],
        gender: i % 2 === 0 ? "Nam" : "Nữ",
        faculty: "Khoa Công Nghệ Thông Tin"
      };
      newStudents.push(newStudent);
    }
    setStudents([...newStudents,...students]);
  };

  // Tìm kiếm sinh viên trong bảng
  const searchStudentByKeyWord = (keyWord) => {
    // Nếu từ khóa không tồn tại hoặc rỗng, hiển thị tất cả sinh viên
    if (!keyWord || keyWord === "") {
      setSearchStudents([]);
      setFlagRenderBorad(true);
      return;
    }
  
    // Tìm sinh viên theo từ khóa
    const filteredStudents = students.filter((student) =>
      Object.values(student).some((value) => 
        typeof value === 'string' && value.toLowerCase().includes(keyWord.toLowerCase())
      )
    );
  
    // Cập nhật danh sách sinh viên hiển thị
    setSearchStudents(filteredStudents);
    setFlagRenderBorad(false);
  };
  

 const addNewStudentList = (studentNew) => {
  // Kiểm tra xem tất cả các thuộc tính đã khác rỗng hay chưa
  const values = Object.values(studentNew);
  console.log(`Value: ${values}`);
  var isAllValuesNotEmpty = true;
  values.forEach(value => {
    if (value === "" || value === null) {
      isAllValuesNotEmpty = false;
    }
  });
  if (!isAllValuesNotEmpty) {
    alert('Vui lòng nhập đầy đủ thông tin sinh viên');
    return;
  }
  // Kiểm tra xem id của sinh viên mới có bị trùng không
  // const existingStudent = students.find(student => student.studentId === studentNew.id);
  // if (existingStudent) {
  //   alert('Mã sinh viên đã tồn tại');
  // } else {
  //   // Thêm mới sinh viên vào mảng students
  //   setStudents([studentNew, ...students]);
  // }
};


  // thiết lập trạng thái cho sinh viên được sửa
  const handleEditClick = (student) => {
    
    setEditingStudent(student);
  }
  const  changeDataMethod = () => {
    // studentNew
    // setFlagRenderBorad(true);
    // studentNew.birthday = new Date(studentNew.birthday).toLocaleDateString("en-GB");
    // const updatedStudents = students.map(student =>
    //   student.studentId === studentNew.studentId ? { ...studentNew } : student
    // );
    // setStudents(updatedStudents);
    setIsDataChanged(!isDataChanged);
  }

  /// Đánh dấu các sinh viên được chọn
const handleSelect = (id) => {
  // const updatedStudents = 
  // students.map(student =>
  //   student.studentId === id ? { ...student, isSelected: !student.isSelected } : student
  // );
  for (let i = 0; i < students.length; i++) {
   if (students[i].studentId === id) {
      listcheckbox[i] = true;
      break;
   }
  }
  // setStudents(updatedStudents);
  // console.log(updatedStudents);
};

// Xóa các sinh viên đã chọn
const deleteSelectedStudents = () => {
  // const selectedStudentIds = students.filter(student => student.isSelected).map(student => student.id);
  var ids = [];
  for (let i = 0; i < listcheckbox.length; i++) {
    if (listcheckbox[i]) {
      ids = [...ids, students[i].studentId ];
    }
   }
  console.log(ids); 
  const data = {data: ids};
  axios.delete('https://localhost:7181/api/Student',data)
  .then(response => {
    // handle response
    console.log("Xóa thành công các student được chọn!");
    changeDataMethod();
  })
  .catch(error => {
    // handle error
    console.log(error);
  });
  // setStudents(remainingStudents);
  
};

  // Xóa sinh viên theo id
  const deleteStudent = (studentId) => {
    setFlagRenderBorad(true);
    // const remainingStudents = students.filter((student) => student.studentId !== studentId);
    // setStudents(remainingStudents);
    axios.delete(`https://localhost:7181/api/Student/${studentId}`)
  .then(response => {
    // handle response
    console.log("Đã xóa student có id "+studentId);
    changeDataMethod();
  })
  .catch(error => {
    // handle error
    console.log(error);
  });

  };

  return (
    <div className="container">
  <FormSearch searchStudentFunc = {searchStudentByKeyWord}  />
     <br/>
  <InputForm editingStudent={editingStudent} changeDataMethod={changeDataMethod} addNewStudentFunc={addNewStudentList} />
  <br/>
  <div className="buttons-container">
    <button className="button" onClick={addRandomStudents}>Thêm 10 sinh viên ngẫu nhiên</button>
    <button className="button" onClick={deleteSelectedStudents}>Xóa sinh viên đã chọn</button>
  </div>
  <table className="center" >
    <thead>
      <tr>
        <th></th>
        <th className="id-column">Mã SV</th>
        <th className="name-column">Tên sinh viên</th>
        <th className="birthday-column">Ngày sinh</th>
        <th className="gender-column">Giới tính</th>
        <th className="faculty-column">Khoa</th>
        <th className="edit-column">Edit</th>
        
        </tr>
      </thead>
      <tbody>
        {(flagRenderBorad ? students : searchStudents).map((student) => (
          <tr key={student.studentId}>
            <td><input type="checkbox" checked={student.isSelected} onChange={() => handleSelect(student.studentId)} /></td>
            <td>{student.studentId }</td>
            <td>{student.name}</td>
            <td>{student.birthdate}</td>
            <td>{student.gender ? 'nam' : 'nữ'}</td>
            <td>{student.facultyName}</td>
            <td>
              <div style={{display: "flex"}}>  <button className="edit-button" onClick={() => handleEditClick(student)}>Sửa</button>
              <button className="delete-button" onClick={() => deleteStudent(student.studentId)}>Xóa</button> </div>
             
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
  );  
}

export default StudentList;
