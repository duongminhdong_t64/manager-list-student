import React, { useState } from 'react';
import './css/SearchBar.css'

const FormSearch = ({ searchStudentFunc}) => {
  const [keyword, setKeyword] = useState('');

  const handleKeywordChange = (event) => {
    setKeyword(event.target.value);
  };

   //  search sinh vien
  const handleSearch = (keyword) => {
    searchStudentFunc(keyword);
  };

  return (
    <div className='divSearch'>
       <form>
       <label htmlFor="txtTuKhoa" className='labelSearch'>Từ khóa</label>
      <input
        type="text"
        id="txtTuKhoa"
        name="txtTuKhoa"
        placeholder="Từ khóa cần tìm"
        value={keyword}
        onChange={handleKeywordChange}
      />
      <button type="button" onClick={() =>  handleSearch(keyword)}>
        Tìm kiếm
      </button>
    </form>
    </div>
   
  );
};

export default FormSearch;
